<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/action',
        __DIR__ . '/base',
        __DIR__ . '/exec',
        __DIR__ . '/formulaires',
        __DIR__ . '/genie',
        __DIR__ . '/inc',
        __DIR__ . '/plugins',
        __DIR__ . '/prive',
        __DIR__ . '/teleporter',
		__DIR__ . '/svp_administration.php',
		__DIR__ . '/svp_fonctions.php',
		__DIR__ . '/svp_ieconfig.php',
		__DIR__ . '/svp_pipelines.php',
    ]);

	$rectorConfig->sets([
		LevelSetList::UP_TO_PHP_81
	]);
};
