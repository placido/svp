# Changelog
## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev

### Fixed

- #4913 Tenir compte de l'autorisation de configuration d'un plugin pour afficher le bouton Configurer
